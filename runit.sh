#####################################
# cd /home/netdb/git/phishme
# git pull 
k5start -u service/iso-phishme@stanford.edu -f /etc/iso-phishme.keytab -t && ldapsearch -H ldaps://ldap.stanford.edu:636 -b cn=organizations,dc=stanford,dc=edu "(&(suRegID=*))" -o ldif-wrap=no -Y GSSAPI > orgs.out

## Working staff people call
##### Works to exclude nonactive accounts
# Staff
# k5start -u service/iso-phishme@stanford.edu -f /etc/iso-phishme.keytab -t && ldapsearch -H ldaps://ldap.stanford.edu:636 -b cn=people,dc=stanford,dc=edu "(&(uid=*)(suAffiliation=*staff*)(!(suAffiliation=*nonactive*))(!(suAffiliation=*student*)))" uid suAffiliation suDisplayNameFirst suDisplayNameLast suRegisteredName suRegisteredNameLF suSunetID mail suPrimaryOrganizationID ou suPrimaryOrganizationName suGwAffiliation1 suGwAffilCode1 -o ldif-wrap=no -Y GSSAPI | grep -v "^#" > staff_people.out
k5start -u service/iso-phishme@stanford.edu -f /etc/iso-phishme.keytab -t && ldapsearch -H ldaps://ldap.stanford.edu:636 -b cn=people,dc=stanford,dc=edu "(&(uid=*)(suAffiliation=*staff*)(!(suAffiliation=*nonactive*))(!(suAffiliation=*student*)))" uid suDisplayNameFirst suDisplayNameLast suSunetID mail suPrimaryOrganizationID -o ldif-wrap=no -Y GSSAPI | grep -v "^#" > staff_people.out

# Faculty
# k5start -u service/iso-phishme@stanford.edu -f /etc/iso-phishme.keytab -t && ldapsearch -H ldaps://ldap.stanford.edu:636 -b cn=people,dc=stanford,dc=edu "(&(uid=*)(suAffiliation=*faculty*)(!(suAffiliation=*nonactive*))(!(suAffiliation=*student*)))" uid suAffiliation suDisplayNameFirst suDisplayNameLast suRegisteredName suRegisteredNameLF suSunetID mail suPrimaryOrganizationID ou suPrimaryOrganizationName suGwAffiliation1 suGwAffilCode1 -o ldif-wrap=no -Y GSSAPI | grep -v "^#" > fac_people.out
k5start -u service/iso-phishme@stanford.edu -f /etc/iso-phishme.keytab -t && ldapsearch -H ldaps://ldap.stanford.edu:636 -b cn=people,dc=stanford,dc=edu "(&(uid=*)(suAffiliation=*faculty*)(!(suAffiliation=*nonactive*))(!(suAffiliation=*student*)))" uid suDisplayNameFirst suDisplayNameLast suSunetID mail suPrimaryOrganizationID -o ldif-wrap=no -Y GSSAPI | grep -v "^#" > fac_people.out

## Working staff accounts call
# k5start -u service/iso-phishme@stanford.edu -f /etc/iso-phishme.keytab -t && ldapsearch -H ldaps://ldap.stanford.edu:636 -b cn=accounts,dc=stanford,dc=edu "(&(uid=*)(suPrivilegeGroup=stanford:staff-active))" uid suPrivilegeGroup suSeasLocal suEmailStatus suSeasStatus suKerberosStatus suMailDrop suSeasSunetIDPreferred -o ldif-wrap=no -Y GSSAPI | grep -v "^#" > staff_accounts.out
# k5start -u service/iso-phishme@stanford.edu -f /etc/iso-phishme.keytab -t && ldapsearch -H ldaps://ldap.stanford.edu:636 -b cn=accounts,dc=stanford,dc=edu "(&(uid=*)(suPrivilegeGroup=stanford:faculty-active))" uid suPrivilegeGroup suSeasLocal suEmailStatus suSeasStatus suKerberosStatus suMailDrop suSeasSunetIDPreferred -o ldif-wrap=no -Y GSSAPI | grep -v "^#" > faculty_accounts.out

### Trimmed to exclude suPrivelegeGroup because it's a lot of noise.
# Staff (active)
# k5start -u service/iso-phishme@stanford.edu -f /etc/iso-phishme.keytab -t && ldapsearch -H ldaps://ldap.stanford.edu:636 -b cn=accounts,dc=stanford,dc=edu "(&(uid=*)(suPrivilegeGroup=stanford:staff-active))" uid suSeasLocal suEmailStatus suSeasStatus suKerberosStatus: active suMailDrop suSeasSunetIDPreferred -o ldif-wrap=no -Y GSSAPI | grep -v "^#" > staff_accounts.out
k5start -u service/iso-phishme@stanford.edu -f /etc/iso-phishme.keytab -t && ldapsearch -H ldaps://ldap.stanford.edu:636 -b cn=accounts,dc=stanford,dc=edu "(&(uid=*)(suPrivilegeGroup=stanford:staff-active))" uid suEmailStatus suSeasStatus suMailDrop suSeasSunetIDPreferred -o ldif-wrap=no -Y GSSAPI | grep -v "^#" > staff_accounts.out
# Faculty (active)
# k5start -u service/iso-phishme@stanford.edu -f /etc/iso-phishme.keytab -t && ldapsearch -H ldaps://ldap.stanford.edu:636 -b cn=accounts,dc=stanford,dc=edu "(&(uid=*)(suPrivilegeGroup=stanford:faculty-active))" uid suSeasLocal suEmailStatus suSeasStatus suKerberosStatus: active suMailDrop suSeasSunetIDPreferred -o ldif-wrap=no -Y GSSAPI | grep -v "^#" > fac_accounts.out
k5start -u service/iso-phishme@stanford.edu -f /etc/iso-phishme.keytab -t && ldapsearch -H ldaps://ldap.stanford.edu:636 -b cn=accounts,dc=stanford,dc=edu "(&(uid=*)(suPrivilegeGroup=stanford:faculty-active))" uid suEmailStatus suSeasStatus suMailDrop suSeasSunetIDPreferred -o ldif-wrap=no -Y GSSAPI | grep -v "^#" > fac_accounts.out

pwsh /home/netdb/git/phishme/ldif_parse.ps1
