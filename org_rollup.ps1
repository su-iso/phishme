#
$DebugPreference = "Continue"

$script_home = Split-Path -Path $MyInvocation.MyCommand.Definition -Parent

## Read the ldif data into variables
## Note this uses a delimiter 'dn' in the text to delimit object boundaries
## ...so 'dn' gets stripped from the resulting object / blob-of-text.
grep -v "^#" "$script_home/orgs.out" > "$script_home/orgs.nohash.out"

#######  A typical org record as returned by the current query and stored in orgs.out.
#######  ...imagine it without the leading "# " if possible.  :)
# # 00000000000000000103ab400b0baa77, organizations, stanford.edu
# dn: suRegID=00000000000000000103ab400b0baa77,cn=organizations,dc=stanford,dc=edu
# displayName: School of Humanities and Sciences
# suRegisteredName: School of Humanities and Sciences
# suAdminID: PAAA
# suUniqueIdentifier: DR012U241
# telephoneNumber: (650) 723-2275
# suParentOrgRegID: 00000000000000000001ab400b0baa77
# suRegID: 00000000000000000103ab400b0baa77
# suDisplayName30: Humanities and Sciences
# suChildOrgRegID: 13fbb188cf224bf88892c095016bb316
# suChildOrgRegID: 12db1c361d7a49bf9c23f5e1428b1dde

$orgsfile_path = "$script_home/orgs.nohash.out"
$orgs_ldif = 'null'
# Write-Debug "before the orgs_ldif:  $orgs_ldif"
$orgs_ldif = Get-Content -Path $orgsfile_path -Delimiter dn
# Write-Debug "before the orgs_ldif:  $orgs_ldif"

## Make new files to overwrite the last run.
New-Item "$script_home/orgs_expanded.txt" -ItemType file -Force
New-Item "$script_home/orgs_rebase.txt" -ItemType file -Force
New-Item "$script_home/orgs_full_rebase.txt" -ItemType file -Force
New-Item "$script_home/current_data/orgs.csv" -ItemType file -Force

$org_summary_list = New-Object 'System.Collections.Generic.List[String]'

$summary_list = New-Object 'System.Collections.Generic.List[String]'


## Cat the output of the people search and do stuff to each object.
$orgs_ldif | Foreach-Object {
   $oneline = @{"displayName" = "nulldisplayName"; "suAdminID" = "null"; "suUniqueIdentifier" = "null";"suParentOrgRegID" = "null";"suRegID" = "null"; "my_lev2_org" = "null"}
   ## The beginning of each record starts with ':', because the 'dn' got stripped in the Get-Content above.
   if ($_.contains(':')) {
   	$_ -replace "^: ","dn=" -replace ": ","="| grep -Ev "displayName30|dn=" | grep -E "displayName|suAdminID|suUniqueIdentifier|suParentOrgRegID|suRegID|my_lev2_org" | ForEach-Object {
  	$parts = $_.split('=') 
  	$oneline[$parts[0]] = $parts[1]
  	}
   ## Needs to be written to an object so that it can be pushed out through Export-Csv below without issue.
   $new_obj = New-Object PSObject -Property $oneline
   $new_obj | Export-Csv "$script_home/orgs_rebase.txt" -Append
   # Write-Debug "summary_list:  $summary_list"
   }
}

$content = Import-Csv "$script_home/orgs_rebase.txt" 
$content2 = Import-Csv "$script_home/orgs_rebase.txt" 

function get-Parent {
	param ($xsuRegID)
 	# Write-Debug "xsuRegID:  $xsuRegID"
    	# Start-Sleep -s 2
        $parent_id = 'null'
	$content2 | ForEach-Object {
	if ($_.suRegID -eq $xsuRegID){
 	   # Write-Host "_.suRegID " $_.suRegID "equals xsuregid: " $xsuRegID 
	   $parent_id = $_.suParentOrgRegID 
	   $display_name = $_.displayName 
 	   # Write-Debug "$_.displayName suParentOrgRegID:  $_.suParentOrgRegID"
 	   # Write-Host "Returning " + $_.suParentOrgRegID + "as " + "suParentOrgRegID for: " $_.displayName 
 	   #$ Write-Host "Returning " $parent_id "as suParentOrgRegID for: " $display_name 
           # Start-Sleep -s 2
	   # return $_.suParentOrgRegID
	   return $parent_id
	   }
	}
}


# $bbbbborg_summary_list = New-Object 'System.Collections.Generic.List[String]'

$level0_org = 'null'
# $level1_org = 'null'
$level1_orgs = New-Object 'System.Collections.Generic.List[String]'
$level2_orgs = New-Object 'System.Collections.Generic.List[String]'
$level2_orgs_and_names = New-Object 'System.Collections.Generic.List[String]'
$level2_orgs_hash = @{}

$content | ForEach-Object{
	if ($_.displayName -eq 'Stanford Universe') { 
		$level0_org = $_.suRegID
	}
}

$content | ForEach-Object{
	if ($_.suParentOrgRegID -eq $level0_org) { 
		$level1_orgs.Add($_.suRegID)
	}
}
$content | ForEach-Object{
	if ($level1_orgs -contains $_.suParentOrgRegID) { 
		$level2_orgs.Add($_.suRegID)
		$level2_orgs_and_names.Add("$_.suRegID,$_.displayName")
	}
}

$level2_orgs > "$script_home/level2_orgs.out"
$level2_orgs_and_names > "$script_home/level2_orgs_and_names.out"


$content | ForEach-Object{
   $this_gparent = 'null'
   $this_ggparent = 'null'
   $this_gggparent = 'null'
   $this_ggggparent = 'null'
   $my_parents = New-Object 'System.Collections.Generic.List[String]'
   $my_level2_org = 'null' 
   $foo = $_.displayName
   $suregid = $_.suRegID
   $xsuRegID = 'null'
   $xsuRegID = $_.suParentOrgRegID
   if ($level1_orgs -contains $_.suRegID) {
	$my_level2_org = 'NA'
 	# Write-Debug "I am a level 1 org:  $foo, $my_level2_org"
        return
   } elseif ($level2_orgs -contains $_.suRegID) {
	$my_level2_org = $suregid
 	# Write-Debug "I am a level 3 org:  $foo, $my_level2_org"
	$one_line = @{"displayName" = $_.displayName; "suAdminID" = $_.suAdminID; "suUniqueIdentifier" = $_.suUniqueIdentifier; "suParentOrgRegID" = $_.suParentOrgRegID; "suRegID" = $_.suRegID; "my_lev2_org" = $my_level2_org;}
   } elseif ($level2_orgs -contains $xsuRegID) {
	$my_level2_org = $xsuRegID
	$one_line = @{"displayName" = $_.displayName; "suAdminID" = $_.suAdminID; "suUniqueIdentifier" = $_.suUniqueIdentifier; "suParentOrgRegID" = $_.suParentOrgRegID; "suRegID" = $_.suRegID; "my_lev2_org" = $my_level2_org;}
   } else {
	if ($xsuRegID -ne 'null') {
	   $this_gparent = get-Parent $xsuRegID
	}
	if ($level2_orgs -contains $this_gparent) {
	   $my_level2_org = $this_gparent
	   # Write-Debug "---my level 2 org is: $my_level2_org"
	   $one_line = @{"displayName" = $_.displayName; "suAdminID" = $_.suAdminID; "suUniqueIdentifier" = $_.suUniqueIdentifier; "suParentOrgRegID" = $_.suParentOrgRegID; "suRegID" = $_.suRegID; "my_lev2_org" = $my_level2_org;}
	} else {
	
	if ($xsuRegID -ne 'null' -AND $this_gparent -ne 'null') {
	   $this_ggparent = get-Parent $this_gparent 
	}
	   if ($level2_orgs -contains $this_ggparent) {
 		# Write-Debug "---my level 2 org is: $my_level2_org"
		$my_level2_org = $this_ggparent
		$one_line = @{"displayName" = $_.displayName; "suAdminID" = $_.suAdminID; "suUniqueIdentifier" = $_.suUniqueIdentifier; "suParentOrgRegID" = $_.suParentOrgRegID; "suRegID" = $_.suRegID; "my_lev2_org" = $my_level2_org;}
	   } else {
		if ($xsuRegID -ne 'null' -AND $this_ggparent -ne 'null') {
	        $this_gggparent = get-Parent $this_ggparent
	   }
	        # $this_gggparent = get-Parent $xsuRegID
	        if ($level2_orgs -contains $this_gggparent) {
		   $my_level2_org = $this_gggparent
 		   # Write-Debug "---my level 2 org is: $my_level2_org"
		   $one_line = @{"displayName" = $_.displayName; "suAdminID" = $_.suAdminID; "suUniqueIdentifier" = $_.suUniqueIdentifier; "suParentOrgRegID" = $_.suParentOrgRegID; "suRegID" = $_.suRegID; "my_lev2_org" = $my_level2_org;}
		} else {
		if ($xsuRegID -ne 'null' -AND $this_gggparent -ne 'null') {
	           $this_ggggparent = get-Parent $this_gggparent
		}
	           if ($level2_orgs -contains $this_ggggparent) {
		      $my_level2_org = $this_ggggparent
 		      # Write-Debug "---expecting level 1 grandparent:  $my_level2_org"
		      $one_line = @{"displayName" = $_.displayName; "suAdminID" = $_.suAdminID; "suUniqueIdentifier" = $_.suUniqueIdentifier; "suParentOrgRegID" = $_.suParentOrgRegID; "suRegID" = $_.suRegID; "my_lev2_org" = $my_level2_org;}
		   }
	       	}
	   }
	}
     }
     $new_obj = New-Object PSObject -Property $one_line
     # Write-Host "new_obj is:" $new_obj
     $new_obj | Export-Csv "$script_home/orgs_full_rebase.txt" -Append 

}


$nextround = Import-Csv "$script_home/orgs_full_rebase.txt" 
$nextround2 = Import-Csv "$script_home/orgs_full_rebase.txt" 

$nextround | ForEach-Object {
   # $my_parents = New-Object 'System.Collections.Generic.List[String]'
   $my_l2_suAdminID = 'null'
   $my_l2_orgname = 'null'
   $my_l2_org = $_.my_lev2_org
   ########################
   ## Selector1
   # NOTE:  this selector is different than the one below because both comparators
   # are from *this* selection level.  The next selector has a comparator from 
   # this level and its own level.  This one is specifically for level2 orgs.
   if ($my_l2_org -eq $_.suRegID) {
      $my_l2_suAdminID = $_.suAdminID
      $my_l2_orgname = $_.displayName
   }
   ########################
   ## Selector2
   $nextround2 | ForEach-Object {
   	if ($my_l2_org -eq $_.suRegID) {
   	   $my_l2_suAdminID = $_.suAdminID
   	   $my_l2_orgname = $_.displayName
	}
   }	
   ########################
   $one_line = @{"org_name" = $_.displayName; "suAdminID" = $_.suAdminID; "my_l2_suAdminID" = $my_l2_suAdminID; "suParentOrgRegID" = $_.suParentOrgRegID; "suRegID" = $_.suRegID; "my_lev2_org" = $my_l2_org; "my_l2_orgname" = $my_l2_orgname;}
   $new_obj = New-Object PSObject -Property $one_line
   # Write-Host "new_obj is:" $new_obj
   $new_obj | Export-Csv "$script_home/current_data/orgs.csv" -Append 
}

. "$script_home/reconcile.ps1"
