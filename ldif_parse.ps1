#
$DebugPreference = "Continue"

$script_home = Split-Path -Path $MyInvocation.MyCommand.Definition -Parent

## Read the ldif data into variables
## Note this uses a delimiter 'dn' in the text to delimit object boundaries
## ...so 'dn' gets stripped from the resulting object / blob-of-text.
$staff_people_ldif=Get-Content "$script_home/staff_people.out" -Delimiter dn
$staff_accounts_ldif=Get-Content "$script_home/staff_accounts.out" -Delimiter dn


$faculty_people_ldif=Get-Content "$script_home/fac_people.out" -Delimiter dn
$faculty_accounts_ldif=Get-Content "$script_home/fac_accounts.out" -Delimiter dn

## Make new files to overwrite the last run.
New-Item "$script_home/current_data/staff_people_runtime_summary.txt" -ItemType file -Force
New-Item "$script_home/current_data/staff_accounts_runtime_summary.txt" -ItemType file -Force
New-Item "$script_home/current_data/faculty_people_runtime_summary.txt" -ItemType file -Force
New-Item "$script_home/current_data/faculty_accounts_runtime_summary.txt" -ItemType file -Force

## Initialize the summary list for roll-up reporting.
$people_summary_list = New-Object 'System.Collections.Generic.List[String]'
$accounts_summary_list = New-Object 'System.Collections.Generic.List[String]'

$summary_list = New-Object 'System.Collections.Generic.List[String]'
$fac_people_summary_list = New-Object 'System.Collections.Generic.List[String]'
$staff_people_summary_list = New-Object 'System.Collections.Generic.List[String]'
$fac_accounts_summary_list = New-Object 'System.Collections.Generic.List[String]'
$staff_accounts_summary_list = New-Object 'System.Collections.Generic.List[String]'

## Cat the output of the people search and do stuff to each object.
$staff_people_ldif | Foreach-Object {
 		$oneline = @{"mail" = "nullmail"; "suPrimaryOrganizationID" = "null"; "suPrimaryOrganizationName" = "null";"suGwAffiliation1" = "null";"uid" = "null";"suDisplayNameFirst" = "null";"suDisplayNameLast" = "null"}
 		# Write-Debug "main:[$_]"
 		# Start-Sleep -s 2

		## The beginning of each record starts with ':', because the 'dn' got stripped in the Get-Content above.
 		if ($_.contains(':')) {
 			$_ -replace "^: ","dn=" -replace ": ","="| grep -E "uid|mail|suDisplayName|suGwAff|suPrimary" | ForEach-Object {
 			$parts = $_.split('=') 
 			$oneline[$parts[0]] = $parts[1]
 			}
		## Needs to be written to an object so that it can be pushed out through Export-Csv below without issue.
 		$new_obj = New-Object PSObject -Property $oneline
 		$new_obj | Export-Csv "$script_home/current_data/staff_people_runtime_summary.txt" -Append
 		}
 	}

$faculty_people_ldif | Foreach-Object {
 		$oneline = @{"mail" = "nullmail"; "suPrimaryOrganizationID" = "null"; "suPrimaryOrganizationName" = "null";"suGwAffiliation1" = "null";"uid" = "null";"suDisplayNameFirst" = "null";"suDisplayNameLast" = "null"}
 		# Start-Sleep -s 2
		## The beginning of each record starts with ':', because the 'dn' got stripped in the Get-Content above.
 		if ($_.contains(':')) {
 			$_ -replace "^: ","dn=" -replace ": ","="| grep -E "uid|mail|suDisplayName|suGwAff|suPrimary" | ForEach-Object {
 			$parts = $_.split('=') 
 			$oneline[$parts[0]] = $parts[1]
 			}
		## Needs to be written to an object so that it can be pushed out through Export-Csv below without issue.
 		$new_obj = New-Object PSObject -Property $oneline
		
 		$new_obj | Export-Csv "$script_home/current_data/faculty_people_runtime_summary.txt" -Append
 		}
 	}

$staff_accounts_ldif | Foreach-Object {
		$oneline = @{ "uid" = "null"; "suSeasLocal" = "null"; "suEmailStatus" = "null"; "suSeasStatus" = "null"; "suMailDrop" = "null"; "suSeasSunetIDPreferred" = "null" }
		if ($_.contains(':')) {
			$_ -replace "^: ","dn=" -replace ": ","="| grep -E "uid|mail|suDisplayName|suGwAff|suPrimary" | grep -v "dn="| ForEach-Object {
			$parts = $_.split('=') 
			$oneline[$parts[0]] = $parts[1]
			}
		$new_obj = New-Object PSObject -Property $oneline
		$new_obj | Export-Csv "$script_home/current_data/staff_accounts_runtime_summary.txt" -Append
		}
	}

$faculty_accounts_ldif | Foreach-Object {
		$oneline = @{ "uid" = "null"; "suSeasLocal" = "null"; "suEmailStatus" = "null"; "suSeasStatus" = "null"; "suMailDrop" = "null"; "suSeasSunetIDPreferred" = "null" }
		if ($_.contains(':')) {
			$_ -replace "^: ","dn=" -replace ": ","="| grep -E "uid|mail|suDisplayName|suGwAff|suPrimary" | grep -v "dn="| ForEach-Object {
			$parts = $_.split('=') 
			$oneline[$parts[0]] = $parts[1]
			}
		$new_obj = New-Object PSObject -Property $oneline
		$new_obj | Export-Csv "$script_home/current_data/faculty_accounts_runtime_summary.txt" -Append
		}
	}

. "$script_home/org_rollup.ps1" 
