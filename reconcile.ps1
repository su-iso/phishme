#
$DebugPreference = "Continue"
$this_path = Split-Path -Path $MyInvocation.MyCommand.Definition -Parent
$this_run = Get-Date -Format FileDateTime
# New-Item "$this_path/$this_run" -ItemType Directory -Force
$input_home = "$this_path/current_data" 
$output_home = "$this_path/$this_run" 
# New-Item "$output_home" -ItemType Directory -Force
$summary_list = New-Object 'System.Collections.Generic.List[String]'
New-Item "$output_home" -ItemType Directory

Copy-Item "$input_home/orgs.csv" -Destination $output_home
Copy-Item "$input_home/staff_accounts_runtime_summary.txt" -Destination $output_home
Copy-Item "$input_home/staff_people_runtime_summary.txt" -Destination $output_home
Copy-Item "$input_home/faculty_people_runtime_summary.txt" -Destination $output_home
Copy-Item "$input_home/faculty_accounts_runtime_summary.txt" -Destination $output_home

$staff_people = Import-Csv "$input_home/staff_people_runtime_summary.txt"
# "suGwAffiliation1","mail","suPrimaryOrganizationID","uid","suDisplayNameLast","suDisplayNameFirst","suPrimaryOrganizationName","suAffilJobDescription"
# "University|Staff|Stanford Cancer Institute|Lab Manager","fzhao@stanford.edu","VTFW","fzhao","Zhao","Feifei","Stanford Cancer Institute","more stuff"

$staff_accounts = Import-Csv "$input_home/staff_accounts_runtime_summary.txt"
# "suMailDrop","uid","suSeasLocal","suSeasStatus","suSeasSunetIDPreferred","suEmailStatus"
# "null","aali","null","null","null","active"

$orgs = Import-Csv "$output_home/orgs.csv"
# "my_l2_orgname","my_l2_suAdminID","suParentOrgRegID","my_lev2_org","suAdminID","org_name","suRegID"
# "Provost's Office","FAAA","00000000000000000001ab400b0baa77","00000000000000000102ab400b0baa77","FAAA","Provost's Office","00000000000000000102ab400b0baa77"

$fac_people = Import-Csv "$input_home/faculty_people_runtime_summary.txt"
# "suGwAffiliation1","mail","suPrimaryOrganizationID","uid","suDisplayNameLast","suDisplayNameFirst","suPrimaryOrganizationName","suAffilJobDescription"
# "University|Faculty|Health Research and Policy - Epidemiology|Professor","vhenderson@stanford.edu","WDWM","vhenders","Henderson","V.","Epidemiology","more stuff"

$fac_accounts = Import-Csv "$input_home/faculty_accounts_runtime_summary.txt"
# "suMailDrop","uid","suSeasLocal","suSeasStatus","suSeasSunetIDPreferred","suEmailStatus"
# "null","aaiken","null","null","null","active"

## New file to overwrite the last run.
# New-Item "$output_home/staff_phishme.csv" -ItemType file -Force
New-Item "$output_home/fac_phishme.csv" -ItemType file -Force
New-Item "$output_home/staff_phishme.csv" -ItemType file -Force

$staff_people | ForEach-Object {
   $my_uid = $_.uid
   $my_orgname = $_.suPrimaryOrganizationName
   $fname = $_.suDisplayNameFirst
   $lname = $_.suDisplayNameLast
   $my_name = "$fname $lname"
   $my_mailval = $_.mail
   $my_jobdesc = $_.suAffilJobDescription
   $my_primary_orgid = $_.suPrimaryOrganizationID 
   $maildrop = "null"
   $email_status = "null"
   $id_preferred = "null"
   $l2_orgname = "null"
   $l2_suadminid = "null"
   $staff_accounts | ForEach-Object {
	if ($my_uid -eq $_.uid) {
 	   $maildrop = $_.suMailDrop
 	   $email_status = $_.suEmailStatus
 	   $id_preferred = $_.suSeasSunetIDPreferred
 	   return
 	}
    }
    $orgs | ForEach-Object {
 	if ($my_primary_orgid -eq $_.suAdminid) {
 	   $l2_orgname = $_.my_l2_orgname
 	   $l2_suadminid = $_.my_l2_suAdminID
 	   $my_orgname = $_.org_name
 	   return
 	}
    }
    # $oneline = @{"displayName" = "nulldisplayName"; "suAdminID" = "null"; "suUniqueIdentifier" = "null";"suParentOrgRegID" = "null";"suRegID" = "null"; "my_lev2_org" = "null"}
    # $oneline = @{"uid" = $my_uid; "name" = $my_name; "mail" = $my_mailval; "gwaffil_1" = $my_gwafill1; "primary_org" = $my_primary_orgid; "maildrop" = $maildrop; "email_status" = $email_status; "id_preferred" = $id_preferred; "l2_org" = $l2_orgname; "l2_org_id" = $l2_suadminid;}
    # $oneline = [ordered] @{ "uid" = $my_uid; "name" = $my_name; "email" = $my_mailval; "department" = $my_primary_orgid; "organization" = $l2_suadminid; "time_zone" = "Pacific Time (US & Canada)"; "my_org" = $my_orgname; "l2_org" = $l2_orgname; "id_preferred" = $id_preferred; "maildrop" = $maildrop; "email_status" = $email_status; "job_desc" = $my_jobdesc; "fname" = $fname; "lname" = $lname; }
    $oneline = [ordered] @{ "uid" = $my_uid; "name" = $my_name; "email" = $my_mailval; "department" = $my_primary_orgid; "organization" = $l2_suadminid; "time_zone" = "Pacific Time (US & Canada)"; "my_org" = $my_orgname; "l2_org" = $l2_orgname; "id_preferred" = $id_preferred; "maildrop" = $maildrop; "email_status" = $email_status; "job_desc" = $my_jobdesc; "fname" = $fname; "lname" = $lname; }
    $new_obj = New-Object PSObject -Property $oneline
    # Write-Host "new_obj is:" $new_obj
    $new_obj | Export-Csv "$output_home/staff_phishme.csv" -Append 
    # $summary_list.Add("$new_obj")
 }

$fac_people | ForEach-Object {
   $my_uid = $_.uid
   $my_orgname = $_.suPrimaryOrganizationName
   $fname = $_.suDisplayNameFirst
   $lname = $_.suDisplayNameLast
   $my_name = "$fname $lname"
   $my_jobdesc = $_.suAffilJobDescription
   $my_mailval = $_.mail
   # $my_gwafill1 = $_.suGwAffiliation1
   $my_primary_orgid = $_.suPrimaryOrganizationID 
   $maildrop = "null"
   $email_status = "null"
   $id_preferred = "null"
   $l2_orgname = "null"
   $l2_suadminid = "null"
   $fac_accounts | ForEach-Object {
	if ($my_uid -eq $_.uid) {
	   $maildrop = $_.suMailDrop
	   $email_status = $_.suEmailStatus
	   $id_preferred = $_.suSeasSunetIDPreferred
	   return
	}
   }
   $orgs | ForEach-Object {
	if ($my_primary_orgid -eq $_.suAdminid) {
	   $l2_orgname = $_.my_l2_orgname
	   $l2_suadminid = $_.my_l2_suAdminID
 	   $my_orgname = $_.org_name
	   return
	}
   }
   # $oneline = @{"uid" = $my_uid; "name" = $my_name; "mail" = $my_mailval; "gwaffil_1" = $my_gwafill1; "primary_org" = $my_primary_orgid; "maildrop" = $maildrop; "email_status" = $email_status; "id_preferred" = $id_preferred; "l2_org" = $l2_orgname; "l2_org_id" = $l2_suadminid;}
   # $oneline = @{"uid" = $my_uid; "name" = $my_name; "mail" = $my_mailval; "primary_org" = $my_primary_orgid; "maildrop" = $maildrop; "email_status" = $email_status; "id_preferred" = $id_preferred; "l2_org" = $l2_orgname; "l2_org_id" = $l2_suadminid; "my_org" = $my_orgname; }
 #   $oneline = [ordered] @{"uid" = $my_uid; "name" = $my_name; "mail" = $my_mailval; "primary_org" = $my_primary_orgid; "maildrop" = $maildrop; "email_status" = $email_status; "id_preferred" = $id_preferred; "l2_org" = $l2_orgname; "l2_org_id" = $l2_suadminid; "my_org" = $my_orgname; }
    # $oneline = [ordered] @{ "uid" = $my_uid; "name" = $my_name; "email" = $my_mailval; "department" = $my_primary_orgid; "organization" = $l2_suadminid; "time_zone" = "Pacific Time (US & Canada)"; "my_org" = $my_orgname; "l2_org" = $l2_orgname; "id_preferred" = $id_preferred; "maildrop" = $maildrop; "email_status" = $email_status; }
    $oneline = [ordered] @{ "uid" = $my_uid; "name" = $my_name; "email" = $my_mailval; "department" = $my_primary_orgid; "organization" = $l2_suadminid; "time_zone" = "Pacific Time (US & Canada)"; "my_org" = $my_orgname; "l2_org" = $l2_orgname; "id_preferred" = $id_preferred; "maildrop" = $maildrop; "email_status" = $email_status; "job_desc" = $my_jobdesc; "fname" = $fname; "lname" = $lname; }
   $new_obj = New-Object PSObject -Property $oneline
   $new_obj | Export-Csv "$output_home/fac_phishme.csv" -Append 
}

head -1 "$output_home/fac_phishme.csv" > "$output_home/fac_phishme_final.csv" 
head -1 "$output_home/staff_phishme.csv" > "$output_home/staff_phishme_final.csv" 

sort "$output_home/fac_phishme.csv" | grep -v "nullmail"| uniq >> "$output_home/fac_phishme_final.csv" 
sort "$output_home/staff_phishme.csv"| grep -v "nullmail" | uniq >> "$output_home/staff_phishme_final.csv" 

